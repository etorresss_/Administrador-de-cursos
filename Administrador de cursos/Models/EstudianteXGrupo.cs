﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Administrador_de_cursos.Models
{
    public class EstudianteXGrupo
    {
        private int IdEstudiante;
        private String Nombre;


        //Constructor
        public EstudianteXGrupo(int IdEstudiante)
        {
            this.IdEstudiante = IdEstudiante;
            Estudiante estudiante = new Estudiante();
            this.Nombre = estudiante.ObtenerInfoEstudiante(IdEstudiante);
            estudiante = null;
        }


        public EstudianteXGrupo(int IdEstudiante, String Nombre)
        {
            this.IdEstudiante = IdEstudiante;
            this.Nombre = Nombre;
        }

        //Métodos
        public int GetIdEstudiante()
        {
            return this.IdEstudiante;
        }
        //-----------------------------------------------------------------------//
        public String GetNombre()
        {
            return this.Nombre;
        }
    }
}