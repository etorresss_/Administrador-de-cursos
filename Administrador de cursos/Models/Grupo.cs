﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Administrador_de_cursos
{
    public class Grupo
    {
        //Atributos
        private int IdGrupo;
        private int IdPeriodo;
        private string Codigo;
        private string Nombre;
        private bool Estado;

        //Constructor
        public Grupo(int IdGrupo, int IdPeriodo, string Codigo, string Nombre, bool Estado)
        {
            this.IdGrupo = IdGrupo;
            this.IdPeriodo = IdPeriodo;
            this.Codigo = Codigo;
            this.Nombre = Nombre;
            this.Estado = Estado;
        }

        //Métodos
        public int GetIdGrupo()
        {
            return this.IdGrupo;
        }

        private void SetIdGrupo(int pIdGrupo)
        {
            this.IdGrupo = pIdGrupo;
        }
        //-----------------------------------------------------------------------//
        public int GetIdPeriodo()
        {
            return this.IdPeriodo;
        }

        private void SetIdPeriodo(int pIdPeriodo)
        {
            this.IdPeriodo = pIdPeriodo;
        }
        //-----------------------------------------------------------------------//
        public bool GetEstado()
        {
            return this.Estado;
        }

        private void SetEstado(bool pEstado)
        {
            this.Estado = pEstado;
        }
        //-----------------------------------------------------------------------//
        public string GetCodigo()
        {
            return this.Codigo;
        }

        private void SetCodigo(string pCodigo)
        {
            this.Codigo = pCodigo;
        }
        //-----------------------------------------------------------------------//
        public string GetNombre()
        {
            return this.Nombre;
        }

        private void SetNombre(string pNombre)
        {
            this.Nombre = pNombre;
        }
        //-----------------------------------------------------------------------//
        public void show()
        {
            Console.WriteLine("IdGrupo: " + this.GetIdGrupo());
            Console.WriteLine("IdPeriodo: " + this.GetIdPeriodo());
            Console.WriteLine("Codigo: " + this.GetCodigo());
            Console.WriteLine("Nombre: " + this.GetNombre());
            Console.WriteLine("Estado: " + this.GetEstado());
            Console.WriteLine("-----------------------------------------------------");
        }
    }
}