﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Administrador_de_cursos.Models
{
    public class Funcionario
    {
        //Atributos
        private int IdFuncionario;
        private string Nombre;
        private List<Grupo> GruposActivos = new List<Grupo>();
        private List<Grupo> GruposInactivos = new List<Grupo>();

        //Constructor
        public Funcionario(int IdFuncionario)
        {
            this.IdFuncionario = IdFuncionario;
            this.Nombre = ObtenerInfoProfe(IdFuncionario);
            this.GruposActivos = ObtenerGruposPorProfesor(IdFuncionario, 1);
            this.GruposInactivos = ObtenerGruposPorProfesor(IdFuncionario, 0);
        }

        //Métodos
        public int GetIdFuncionario()
        {
            return this.IdFuncionario;
        }

        private void SetIdFuncionario(int pIdFuncionario)
        {
            this.IdFuncionario = pIdFuncionario;
        }
        //-----------------------------------------------------------------------//
        public string GetNombre()
        {
            return this.Nombre;
        }

        private void SetNombre(string pNombre)
        {
            this.Nombre = pNombre;
        }
        //----------------------------------------------------------------------//
        public List<Grupo> GetGruposActivos()
        {
            return this.GruposActivos;
        }

        public List<Grupo> GetGruposInactivos()
        {
            return this.GruposInactivos;
        }

        private void SetGrupos()
        {
            this.GruposActivos = ObtenerGruposPorProfesor(this.IdFuncionario, 1);
            this.GruposInactivos = ObtenerGruposPorProfesor(this.IdFuncionario, 0);
        }
        //----------------------------------------------------------------------//
        public void Show()
        {
            Console.WriteLine("IdFuncionario: " + this.GetIdFuncionario());
            Console.WriteLine("Nombre: " + this.GetNombre());
            Console.WriteLine("-----------------------------------------------------");
        }

        // Obtiene el nombre del profesor
        public string ObtenerInfoProfe(int idProfesor)
        {
            SqlConnection sqlConnection = new SqlConnection("Data Source = DESKTOP-4EHU2RC; Initial Catalog = BD_sistemaEscolar; Integrated Security = True");
            SqlCommand cmd = new SqlCommand();
            SqlDataReader reader;
            cmd.CommandText = "Obtener_Info_Profe";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = sqlConnection;
            cmd.Parameters.AddWithValue("IdProfe", idProfesor);
            sqlConnection.Open();
            reader = cmd.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(reader);
            DataRow row;
            Console.WriteLine(dataTable.Rows.Count);
            row = dataTable.Rows[0];
            string a = row[0].ToString() + " " + row[1].ToString();
            sqlConnection.Close();
            return a;
        }

        // Obtiene los grupos con el id de profesor, tipo 1 para activos, 0 para inactivos
        public List<Grupo> ObtenerGruposPorProfesor(int idProfesor, int tipo)
        {
            DataTable dataTable = new DataTable();
            SqlConnection sqlConnection = new SqlConnection("Data Source = DESKTOP-4EHU2RC; Initial Catalog = BD_sistemaEscolar; Integrated Security = True");
            List<Grupo> listaGrupo = new List<Grupo>();
            SqlCommand cmd = new SqlCommand();
            SqlDataReader reader;
            DataRow row;

            cmd.CommandText = "Obtener_GruposPorProfesor";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = sqlConnection;

            cmd.Parameters.AddWithValue("IdProfesor", idProfesor);
            cmd.Parameters.AddWithValue("Class", tipo);

            sqlConnection.Open();
            reader = cmd.ExecuteReader();

            dataTable.Load(reader);

            for (int i = 0; i < dataTable.Rows.Count; i++)
            {
                row = dataTable.Rows[i];
                Grupo grupo = new Grupo(int.Parse(row[0].ToString()), int.Parse(row[1].ToString()), row[2].ToString(), row[3].ToString(), bool.Parse(row[4].ToString()));
                listaGrupo.Add(grupo);
                listaGrupo[i].show();
            }
            sqlConnection.Close();
            return listaGrupo;
        }
    }
}