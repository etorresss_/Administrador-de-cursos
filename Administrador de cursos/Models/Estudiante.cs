﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Administrador_de_cursos.Models
{
    public class Estudiante
    {
        //Atributos
        private int IdEstudiante;
        private string Nombre;
        private List<Grupo> GruposActivos;
        private List<Grupo> GruposInactivos;

        // Direccion de conexion con SQL server
        private static String server = "Data Source = DESKTOP-4EHU2RC; Initial Catalog = BD_sistemaEscolar; Integrated Security = True";

        //Constructor
        public Estudiante()
        {

        }

        public Estudiante(int IdEstudiante)
        {
            this.IdEstudiante = IdEstudiante;
            this.Nombre = ObtenerInfoEstudiante(IdEstudiante);
            this.GruposActivos = ObtenerGruposPorEstudiante(IdEstudiante, 1);
            this.GruposInactivos = ObtenerGruposPorEstudiante(IdEstudiante, 0);
        }

        //Métodos
        public int GetIdEstudiante()
        {
            return this.IdEstudiante;
        }

        private void SetIdEstudiante(int pIdEstudiante)
        {
            this.IdEstudiante = pIdEstudiante;
        }
        //-----------------------------------------------------------------------//
        public string GetNombre()
        {
            return this.Nombre;
        }

        private void SetNombre(string pNombre)
        {
            this.Nombre = pNombre;
        }
        //----------------------------------------------------------------------//
        public List<Grupo> GetGruposActivos()
        {
            return this.GruposActivos;
        }
        //----------------------------------------------------------------------//
        public List<Grupo> GetGruposInactivos()
        {
            return this.GruposInactivos;
        }
        //----------------------------------------------------------------------//

        // Obtiene el nombre del estudiante
        public string ObtenerInfoEstudiante(int IdEstudiante)
        {
            SqlConnection sqlConnection = new SqlConnection(server);
            SqlCommand cmd = new SqlCommand();
            SqlDataReader reader;
            cmd.CommandText = "Obtener_Info_Estud";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = sqlConnection;
            cmd.Parameters.AddWithValue("IdEstu", IdEstudiante);
            sqlConnection.Open();
            reader = cmd.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(reader);
            DataRow row;
            Console.WriteLine(dataTable.Rows.Count);
            row = dataTable.Rows[0];
            string a = row[0].ToString() + " " + row[1].ToString();
            sqlConnection.Close();
            return a;
        }

        // Obtiene los grupos con el id de estudiante, tipo 1 para activos, 0 para inactivos
        public List<Grupo> ObtenerGruposPorEstudiante(int IdEstudiante, int tipo)
        {
            DataTable dataTable = new DataTable();
            SqlConnection sqlConnection = new SqlConnection(server);
            List<Grupo> listaGrupo = new List<Grupo>();
            SqlCommand cmd = new SqlCommand();
            SqlDataReader reader;
            DataRow row;

            cmd.CommandText = "Obtener_Grupos_Estu";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = sqlConnection;

            cmd.Parameters.AddWithValue("IdEstudiante", IdEstudiante);
            cmd.Parameters.AddWithValue("Class", tipo);

            sqlConnection.Open();
            reader = cmd.ExecuteReader();

            dataTable.Load(reader);

            for (int i = 0; i < dataTable.Rows.Count; i++)
            {
                row = dataTable.Rows[i];
                Grupo grupo = new Grupo(int.Parse(row[0].ToString()), int.Parse(row[1].ToString()), row[2].ToString(), row[3].ToString(), bool.Parse(row[4].ToString()));
                listaGrupo.Add(grupo);
                listaGrupo[i].show();
            }
            sqlConnection.Close();
            return listaGrupo;
        }

        // Obtiene una lista con todas las evaluaciones
        public List<Nota> Obtener_Notas_Estu(int idGrupo, int idEstudiante)
        {
            DataTable dataTable = new DataTable();
            SqlConnection sqlConnection = new SqlConnection(server);
            List<Nota> listaNota = new List<Nota>();
            SqlCommand cmd = new SqlCommand();
            SqlDataReader reader;
            DataRow row;

            cmd.CommandText = "Obtener_Notas_Estu";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = sqlConnection;

            cmd.Parameters.AddWithValue("IdGrupo", idGrupo);
            cmd.Parameters.AddWithValue("IdEstudiante", idEstudiante);

            sqlConnection.Open();
            reader = cmd.ExecuteReader();

            dataTable.Load(reader);

            for (int i = 0; i < dataTable.Rows.Count; i++)
            {
                row = dataTable.Rows[i];
                Nota nota = new Nota(row[0].ToString(), row[2].ToString(), row[3].ToString(), row[4].ToString(), row[5].ToString(), row[6].ToString());
                listaNota.Add(nota);
            }
            sqlConnection.Close();
            return listaNota;
        }
    }
}