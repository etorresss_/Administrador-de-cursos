﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Administrador_de_cursos
{
    public class Nota{
        private string idEvaluacion;
        private string nombre;
        private string porcentaje;
        private string obtenido;
        private string porcObtenido;
        private string notaFinal;

        public Nota(string idEvaluacion,string nombre, string porcentaje, string obtenido, string porcObtenido, string notaFinal){
            this.idEvaluacion = idEvaluacion;
            this.nombre = nombre;
            this.porcentaje = porcentaje;
            this.obtenido = obtenido;
            this.porcObtenido = porcObtenido;
            this.notaFinal = notaFinal;
        }

        //Métodos
        public string GetIdEvaluacion()
        {
            return this.idEvaluacion;
        }
        //------------------------------------------------------------
        public string GetNombre()
        {
            return this.nombre;
        }
        //------------------------------------------------------------
        public string GetPorcentaje()
        {
            return this.porcentaje;
        }
        //------------------------------------------------------------
        public string GetObtenido()
        {
            return this.obtenido;
        }
        //------------------------------------------------------------
        public string GetPorcObtenido()
        {
            return this.porcObtenido;
        }
        //------------------------------------------------------------
        public string GetNotaFinal()
        {
            return this.notaFinal;
        }
        //------------------------------------------------------------

    }
}
