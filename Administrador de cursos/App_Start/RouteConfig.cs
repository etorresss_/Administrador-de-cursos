﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Administrador_de_cursos
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                "Actualizar",                                                           //Nombre de la ruta
                "Funcionario/ActualizarNota/{idCurso}/{nombreCurso}/"
                + "{idEstudiante}/{nombreEstudiante}/{idEvaluacion}/{nombreEvaluacion}",//Formato de la ruta
                new { controller = "Funcionario", action = "ActualizarNota" }           //Accion por defecto
            );

            routes.MapRoute(
                "Singned",                                                      //Nombre de la ruta
                "Funcionario/Signed/{nombre}/",                                 //Formato de la ruta
                new { controller = "Funcionario", action = "Signed" }           //Accion por defecto
            );

            routes.MapRoute(
                "Default",                                                      //Nombre de la ruta
                "{controller}/{action}",                                        //Formato de la ruta
                new { controller = "Login", action = "Index" }                  //Accion por defecto
            );

        }
    }
}
