﻿using System.Web;
using System.Web.Mvc;

namespace Administrador_de_cursos
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
