﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Administrador_de_cursos.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        public ActionResult Index()
        {
            ViewBag.error = false;
            return View();
        }

        [HttpPost]
        public ActionResult Ingresar(String user, String password, String tipo)
        {
            int autenticado = -1;

            if (String.IsNullOrWhiteSpace(user) || String.IsNullOrWhiteSpace(password))
            {
                ViewBag.error = true;
                ViewBag.errorMessage = "Llene todos los \ncampos, por favor.";
                return View("Index");
            }
            else
            {
                if (tipo.Equals("Funcionario"))
                    autenticado = ValidarUsuario(user, password, "Profesor");
                else
                    autenticado = ValidarUsuario(user, password, tipo);

                if (autenticado == -1)
                {
                    ViewBag.error = true;
                    ViewBag.errorMessage = "Credenciales incorrectas en " + tipo + "s:";
                    ViewBag.errorDescription = "El usuario no pudo ser validado con las credenciales ingresadas.";
                    return View("Index");
                }
            }
            // Llama al metodo Index en HomeController.cs
            return RedirectToAction("Signed", tipo, new { id = autenticado});
        }

        // Validar funcionario en la base
        public int ValidarUsuario(String correo, String contrasenna, String tipo)
        {
            SqlConnection connection = new SqlConnection("Data Source = DESKTOP-4EHU2RC; Initial Catalog = BD_sistemaEscolar; Integrated Security = True");
            connection.Open();
            SqlCommand command = new SqlCommand("Obtener_Id_" + tipo, connection);
            command.CommandType = CommandType.StoredProcedure;
            SqlParameter paramId = new SqlParameter("Resultado", SqlDbType.Int);
            paramId.Direction = ParameterDirection.Output;
            command.Parameters.Add(paramId);
            command.Parameters.AddWithValue("Email", correo);
            command.Parameters.AddWithValue("Contrasenna", contrasenna);
            int rowsAffected = command.ExecuteNonQuery();
            return Convert.ToInt32(command.Parameters["Resultado"].Value);
        }
    }
}