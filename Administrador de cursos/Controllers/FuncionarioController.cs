﻿using Administrador_de_cursos.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace Administrador_de_cursos.Controllers
{
    public class FuncionarioController : Controller
    {
        /* Cada vez que se cambia de pestaña se crea una nueva instancia,
         * por lo que las variables globales deben ser estaticas. */
        private static Funcionario Usuario;
        private static String source = "Data Source = DESKTOP-4EHU2RC; Initial Catalog = BD_sistemaEscolar; Integrated Security = True";
        
        // GET: Teacher
        public ActionResult Index()
        {
            ViewBag.Nombre = Usuario.GetNombre();
            ViewBag.GruposActivos = Usuario.GetGruposActivos();
            ViewBag.GruposInactivos = Usuario.GetGruposInactivos();

            return View();
        }

        // Accion que se llama al iniciar sesion
        public ActionResult Signed(int id)
        {
            Usuario = new Funcionario(id);

            return RedirectToAction("Index");
        }

        // Acciones que se llaman en la barra
        public ActionResult Periodos()
        {
            return View();
        }

        public ActionResult Salir()
        {
            Usuario = null;
            return RedirectToAction("Index", "Login");
        }

        // Accion cuando se le da click a un grupo
        public ActionResult Curso(int idCurso, String nombreCurso, bool activo)
        {
            ViewBag.Title = nombreCurso;
            ViewBag.IdCurso = idCurso;
            ViewBag.NombreCurso = nombreCurso;
            ViewBag.Activo = activo;
            ViewBag.Estudiantes = ObtenerEstudiantesGrupo(idCurso);
            ViewBag.IdUsuario = Usuario.GetIdFuncionario();
            ViewBag.Nombre = Usuario.GetNombre();

            return View();
        }

        // Accion cuando se le da click a un estudiante
        public ActionResult Estudiante(int idCurso, String nombreCurso, int idEstudiante,
            String nombreEstudiante, bool activo)
        {
            ViewBag.Title = nombreCurso + " / " + nombreEstudiante;
            ViewBag.IdEstudiante = idEstudiante;
            ViewBag.NombreEstudiante = nombreEstudiante;
            ViewBag.IdCurso = idCurso;
            ViewBag.NombreCurso = nombreCurso;
            ViewBag.Activo = activo;
            Estudiante estudiante = new Estudiante();
            ViewBag.Evaluaciones = estudiante.Obtener_Notas_Estu(idCurso, idEstudiante);
            estudiante = null;

            return View();
        }

        public List<EstudianteXGrupo> ObtenerEstudiantesGrupo(int idGrupo)
        {
            DataTable dataTable = new DataTable();
            SqlConnection sqlConnection = new SqlConnection(source);
            List<EstudianteXGrupo> listaEstudiante = new List<EstudianteXGrupo>();
            SqlCommand cmd = new SqlCommand();
            SqlDataReader reader;
            DataRow row;

            cmd.CommandText = "Obtener_Estudiantes_Grupo";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = sqlConnection;

            cmd.Parameters.AddWithValue("IdGrupo", idGrupo);

            sqlConnection.Open();
            reader = cmd.ExecuteReader();

            dataTable.Load(reader);

            for (int i = 0; i < dataTable.Rows.Count; i++)
            {
                row = dataTable.Rows[i];
                EstudianteXGrupo estudiante = new EstudianteXGrupo(int.Parse(row["idEstudiante"].ToString()), row["Nombre"].ToString() + " " + row["Apellido"].ToString());
                listaEstudiante.Add(estudiante);
            }
            sqlConnection.Close();
            return listaEstudiante;
        }

        public ActionResult ActualizarNota(int idCurso, String nombreCurso, int idEstudiante,
            String nombreEstudiante, int idEvaluacion, float notaAnterior, String nombreEvaluacion)
        {
            ViewBag.Title = nombreEstudiante + " / " + nombreEvaluacion;
            ViewBag.IdCurso = idCurso;
            ViewBag.NombreCurso = nombreCurso;
            ViewBag.IdEstudiante = idEstudiante;
            ViewBag.NombreEstudiante = nombreEstudiante;
            ViewBag.IdEvaluacion = idEvaluacion;
            ViewBag.NombreEvaluacion = nombreEvaluacion;
            ViewBag.NotaAnterior = notaAnterior;
            ViewBag.Activo = true;

            return View();
        }

        [HttpPost]
        public ActionResult RegistrarNota(int idCurso, String nombreCurso, int idEstudiante,
            String nombreEstudiante, int idEvaluacion, float notaNueva, String nombreEvaluacion)
        {
            DataTable dataTable = new DataTable();
            SqlConnection sqlConnection = new SqlConnection(source);
            SqlCommand cmd = new SqlCommand();

            cmd.CommandText = "Actualizar_Nota";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = sqlConnection;

            cmd.Parameters.AddWithValue("IdEstudinate", idEstudiante);
            cmd.Parameters.AddWithValue("IdEvaluacion", idEvaluacion);
            cmd.Parameters.AddWithValue("Nota", notaNueva);

            SqlParameter Result = new SqlParameter("Result", SqlDbType.Int);
            Result.Direction = ParameterDirection.Output;
            cmd.Parameters.Add(Result);

            sqlConnection.Open();
            cmd.ExecuteReader();
            sqlConnection.Close();

            Enviar("juanjosesolano@hotmail.com", "Actualizacion de " + nombreEvaluacion,
                "Se ha actualizado su nota para la evaluación" + nombreEvaluacion +
                " del curso " + nombreCurso + ". Su nueva nota es " + notaNueva + "%.");

            Console.WriteLine(idEvaluacion.ToString());
            Console.WriteLine(idEstudiante.ToString());
            Console.WriteLine(Result.ToString());

            return RedirectToAction("Estudiante",
                new
                {
                    idCurso,
                    nombreCurso,
                    idEstudiante,
                    nombreEstudiante,
                    activo = true
                });
        }

        public void Enviar(string destinatoria, string encabezado, string cuerpo)
        {
            MailMessage msg = new MailMessage("evelio08castro@gmail.com", destinatoria, encabezado, cuerpo);
            msg.IsBodyHtml = true;
            SmtpClient sc = new SmtpClient("smtp.gmail.com", 587);
            sc.UseDefaultCredentials = false;
            NetworkCredential cre = new NetworkCredential("evelio08castro@gmail.com", "Je89425196");//your mail password
            sc.Credentials = cre;
            sc.EnableSsl = true;
            sc.Send(msg);
        }
    }
}