﻿using Administrador_de_cursos.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Administrador_de_cursos.Controllers
{
    public class EstudianteController : Controller
    {
        /* Cada vez que se cambia de pestaña se crea una nueva instancia,
         * por lo que las variables globales deben ser estaticas. */
        private static Estudiante Usuario;

        // GET: Teacher
        public ActionResult Index()
        {
            ViewBag.Nombre = Usuario.GetNombre();
            ViewBag.GruposActivos = Usuario.GetGruposActivos();
            ViewBag.GruposInactivos = Usuario.GetGruposInactivos();

            return View();
        }

        // Accion que se llama al iniciar sesion
        public ActionResult Signed(int id)
        {
            Usuario = new Estudiante(id);

            return RedirectToAction("Index");
        }

        // Acciones que se llaman en la barra
        public ActionResult Periodos()
        {
            return View();
        }

        public ActionResult Salir()
        {
            Usuario = null;
            return RedirectToAction("Index", "Login");
        }

        // Accion cuando se le da click a un grupo
        public ActionResult Curso(int idCurso, String nombreCurso)
        {
            ViewBag.Title = nombreCurso;
            ViewBag.Evaluaciones = Usuario.Obtener_Notas_Estu(idCurso, Usuario.GetIdEstudiante());

            return View("Curso");
        }
    }
}
